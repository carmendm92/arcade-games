// INSERISCO LE CELLE IN UNA COSTANTE
const cells = document.querySelectorAll('.cell');
// CREO UNA VARIABILE TURNO CHE COMINCIA CON 0
let turn = 0;
// CREO UNA COSTANTE PER MEMORIZZARE I SEGNI
const cellSigns= [];
// CICLO LE CELLE
for(let i=0; i<cells.length; i++){
    const cell = cells[i];

    // CREO UN EVENTO CHE AL CLICK INSERISCE DEL TESTO
    cell.addEventListener('click', function(){

        // SE C'E' QUALCOSA ALL'INTERNO DELLA POSIZIONE
    if(cellSigns[i]){
        // INTERROMPI FUNZIONE
        return;
    }
    //  INCREMENTO IL TURNO DI UNO
    turn++;

    let sign;
    // SE IL TURNO E' PARI IL TESTO CHE VERRA' INSERITO SARA' X
    if(turn % 2 === 0){
        sign = 'X'
    }else{
        // SE E' DISPARI SARA' O
        sign = 'O'
    }

    // INSERISCO IL TESTO
        cell.innerText = sign;
        // ASSOCIO L'INDICE DELLA CELLA CON IL SEGNO CHE HO INSERITO
        cellSigns[i] = sign;

        let hasWon = checkVictory();

        // se vince
        if(hasWon){
            showAlert(`${sign} ha vinto!`)
            // se nessuno vince
        }else if(turn === 9){
            showAlert(`pareggio`)
        }
    })
}

function checkVictory(){
    const winningCombinations =[
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];

    for(let i = 0; i < winningCombinations.length; i++){
        const combinantion = winningCombinations[i];
        const a = combinantion[0];
        const b = combinantion[1];
        const c = combinantion[2];
        
        if(cellSigns[a] && cellSigns[a] === cellSigns[b] && cellSigns[b] === cellSigns[c]){
            console.log(`combinazione vincente ${a} ${b} ${c}`)
            return true;
        }
    }
    return false;
}