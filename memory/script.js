// inserisco nella costamte grid il div con id grid
const grid = document.querySelector('#grid')
const errorsCounter = document.querySelector('#error');
// creo l'array di cards
const cards = ['alien', 'bug', 'duck', 'rocket', 'spaceship', 'tiktac'];

// con spread (rappresentato da ... si può espandere il contenuto di un array
// in un colpo solo, questo caso creo un clone)
const deck = [...cards, ...cards];

let pick =[];

let errors = 0;

// con .sort() si possono ordinare gli elementi di un array
deck.sort(function(){
    // genero un numero casuale
    return 0.5 - Math.random();
})

// con il ciclo for seleziono tutte le card e assegno la classe
for(let i = 0; i < deck.length; i++){
    const card = document.createElement('div');
    card.classList.add('card1', 'col-3');
    // inserisco la chiave all'interno di una costante
    const cardName = deck[i];
    // inserisco un attribute utilizzando la chiave
    card.setAttribute('data-name', cardName);
    // al click assegno una funzione
    card.addEventListener('click', flipCard);
    grid.appendChild(card);
}

errorsCounter.innerText = errors;
// passo come parametro l'evento
function flipCard(event){
    // recupero l'elemento da cui ha avuto origine l'elemento
    const card = event.target;

    // se la card ha la classe flipped stoppa la funzione
    if(card.classList.contains('flipped')) return;
    // assegno il card-name come classe
    card.classList.add(card.getAttribute('data-name'), 'flipped');

    // aggiungo all'array pick la card che seleziono
    pick.push(card);
    // se la lunghezza di pick è uguale a 2 avvia la funizone checkForMatch
    if (pick.length === 2) {
        checkForMatch();
    }
}

function checkForMatch(){
    // recupero le card selezionate
    const card1 = pick[0];
    const card2 = pick[1];
    // recupero il data-name
    const card1Name = card1.getAttribute('data-name');
    const card2Name = card2.getAttribute('data-name');
    
    // se sono uguali 
    if(card1Name === card2Name){
        checkForWin();
    }else{
        setTimeout(function(){
            // se non sono uguali rimuoviamo le classi
            card1.classList.remove(card1Name,'flipped')
            card2.classList.remove(card2Name,'flipped')
            errors++;
            errorsCounter.innerText = errors;
        }, 500);
    }

    pick= [];
}

function checkForWin(){
    const flippedCards = document.querySelectorAll('.flipped');
    if(flippedCards.length === deck.length){
        showAlert('Hai vinto');
    }
}