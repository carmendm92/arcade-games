// contatore punti
const scoreDisplay  =  document.querySelector('#score-display');
let score = 0;
scoreDisplay.innerText = score;

// contatore tempo
const timerDisplay  =  document.querySelector('#timer-display');
let timeLeft = 40;
timerDisplay.innerText = timeLeft;

// diamo un valore di velocità iniziale
let bugSpeed = 800;

// selezioniamo tutte le celle e le inseriamo in una costante
const cells = document.querySelectorAll('.cell');

// logica per randomizzare i bug in una cella
function randomBug(){
    // puliamo le celle prima di azionare di nuovo la funzione
    removeBug();

    // aumentiamo ,a difficoltà se il giocatore fa tot punti

    if(score>=20){
        bugSpeed = bugSpeed -200;
    }
    // seleziono una cella in modo casuale
    const randomNumber = Math.floor(Math.random() * cells.length);
    const cell = cells[randomNumber];
    // e gli assegno la classe CSS bug
    cell.classList.add('bug');
}

const bugMovemet = setInterval(randomBug, bugSpeed);

// funzione per cancellare la classe che inserisce il bug
function removeBug(){
    for(let i = 0; i < cells.length; i++){
        const cellToClean = cells[i];
        cellToClean.classList.remove('bug');
    }
}

// schiacciamo il bug

// seleziono di volta in volta rtutte le celle
for (let i = 0; i < cells.length; i++){
    const cell = cells[i];
    // al click della cella eseguo le funzioni
    cell.addEventListener('click', function(){
        if(cell.classList.contains('bug')){
            // aumento il punteggio
            score++;
            scoreDisplay.innerText = score;
            // cambio il background
            cell.classList.add('splat');
            
            // dopo 200millesecondi cancello il background
            setTimeout(function(){
                cell.classList.remove('splat');
            }, 200);
        }
    })
}

// facciamo un conto alla revescia alla fine del wuale blocchiamo il gioco e lanciamo l'allert
const timer = setInterval(function(){
    timeLeft--;
    timerDisplay.innerText= timeLeft;
    if(timeLeft === 0){
        clearInterval(timer);
        clearInterval(bugMovemet);
        removeBug();

        showAlert(`Game Over! ${score} punti`);
    }
}, 1000)