// selezioniamo i div che saranno la nostra strada
const road = document.querySelectorAll('#grid > div');
const scoreEl = document.querySelector('#score-display');

const duckIdx = 1;
const duck = road[duckIdx];
road[duckIdx].classList.add('duck');

let speed = 300;
let score = 0;



// FUNZIONE CHE MOSTRA LA PIANTA
function addPlant(){
    let currentPlantIdx = road.length - 1;
    road[currentPlantIdx].classList.add('plant');
    
    // salvo nella costante i riferimenti all'elelemento corrente della papera
    
    
    const plantIntVal = setInterval (function(){
        score++;
        scoreEl.innerText = score;

        if (score % 50 === 0){
            speed = speed - 20;
        }
        road[currentPlantIdx].classList.remove('plant');
        currentPlantIdx--;

        if(currentPlantIdx < 0){
            clearInterval(plantIntVal);
            addPlant();
            return;
        }

        if(
            currentPlantIdx === duckIdx &&
            !road[currentPlantIdx].classList.contains('duckjump')
        ){
            road[currentPlantIdx].classList.remove('duck')
            road[currentPlantIdx].classList.add('plant');
            showAlert('Game Over!')
            clearInterval(plantIntVal);
            return;
        }
        road[currentPlantIdx].classList.add('plant');
    }, speed);
}

addPlant();


// FUNZIONE CHE FA SALTARE LA PAPERA
function jump (event){
    // se premiamo spazio e non teniamo premuto
    // if(event.code ==='Space' || event.code === 'touchstart'){
        // assegniamo alla costante duck la classe che la fa translare
        duck.classList.add('duckjump')
        setTimeout(function(){
            duck.classList.remove('duckjump')
        }, 300);
    }


document.addEventListener('touchstart', jump)
document.addEventListener('keydown', jump)